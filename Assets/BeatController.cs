﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatController : MonoBehaviour
{
    public static bool canPressed=false;

    private SpriteRenderer theSR;
    public Sprite defaultImg;
    public Sprite pressImg;
    void Start()
    {
        theSR = GetComponent<SpriteRenderer>();
    }
        void Update()
    {
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "power")
        {
            canPressed = true;
            theSR.sprite = pressImg;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "power")
        {
            theSR.sprite = defaultImg;
            canPressed = false;
        }
    }
}
