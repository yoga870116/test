﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObject : MonoBehaviour
{
    public float beatTempo;
    public bool hasStarted;
    public GameObject targetObj;
    Vector3 targetPosition;
    public GameObject loveTargetObj;
    Vector3 controltargetPosition;

    // Start is called before the first frame update
    void Start()
    {
        targetPosition = targetObj.transform.position;
        controltargetPosition = loveTargetObj.transform.position;
        beatTempo = beatTempo / 60;
    }
    void FixedUpdate()
    {
        targetPosition = targetObj.transform.position;
        controltargetPosition = loveTargetObj.transform.position;
        if (hasStarted)
        {
            if (transform.position.x > controltargetPosition.x-0.5f)//路上
                transform.position -= new Vector3(beatTempo * Time.fixedDeltaTime, 0f, 0f);
            else if (transform.position.x < controltargetPosition.x-0.5f)//回去
            {
                transform.position = targetPosition;
            }
        }
            
    }
    // Update is called once per frame
    void Update()
    {
    }
}
