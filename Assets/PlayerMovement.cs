﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Collision coll;
    [HideInInspector]
    public Rigidbody2D rb;
    //private AnimationScript anim;

    [Space]
    [Header("Stats")]
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;
    public float jumpForce;

    [Space]
    [Header("Effect")]
    private ParticleSystem dashEffect;
    public GameObject particleObject;
    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        dashEffect = particleObject.GetComponent<ParticleSystem>();
    }
    //BeatController.canPressed == true
    // Update is called once per frame
    void FixedUpdate()
    {
        //jump
        if (coll.onGround && Input.GetKeyDown(KeyCode.Space))
        {
            print("jump");
            rb.velocity = Vector2.up * jumpForce;
        }
        //dash
        if (direction == 0)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                direction = 1;
                dashEffect.Play();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                direction = 2;
                dashEffect.Play();
            }
            /* if (Input.anyKeyDown && BeatController.canPressed == false)
             {
                 print("miss");
                 Instantiate(missObj, transform.position + new Vector3(0, 1, 0), transform.rotation);
             }*/
        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            }
            else
                dashTime -= Time.deltaTime;

            if (direction == 1)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                rb.velocity = -Vector2.right * dashSpeed;
            }
            else if (direction == 2)
            {
                transform.eulerAngles = Vector3.zero;
                rb.velocity = Vector2.right * dashSpeed;
            }
        }
    }
}
