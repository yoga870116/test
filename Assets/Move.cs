﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;

    public float jumpForce;
    [SerializeField] private LayerMask whatIsGround;
    public float feePos;

    public Animator animator;
    public GameObject particleObject;
    private ParticleSystem dashEffect;
    public GameObject missObj;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startDashTime = startDashTime;
        dashEffect= particleObject.GetComponent<ParticleSystem>();
        //dashEffect.Stop();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("jump");
            rb.velocity = Vector2.up * jumpForce;
        }
        if (direction == 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) && BeatController.canPressed == true)
            {
                direction = 1;
                dashEffect.Play();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) && BeatController.canPressed == true)
            {
                direction = 2;
                dashEffect.Play();
            }
            if (Input.anyKeyDown && BeatController.canPressed == false)
            {
                print("miss");
                Instantiate(missObj, transform.position+new Vector3(0,1,0), transform.rotation);
            }
        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
                animator.SetBool("run", false);
            }
            else
                dashTime -= Time.deltaTime;

            if (direction == 1)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                animator.SetBool("run", true);
                rb.velocity = -Vector2.right * dashSpeed;
            }
            else if (direction == 2)
            {
                transform.eulerAngles = Vector3.zero;
                animator.SetBool("run", true);
                rb.velocity = Vector2.right * dashSpeed;
            }
        }
    }
}
