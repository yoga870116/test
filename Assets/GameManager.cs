﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioSource theMusic;
    public bool startPlaying;
    public ControlObject[] theBS;
    // Start is called before the first frame update
    void Start()
    {
        startPlaying = true;
        for (int i = 0; i < theBS.Length; i++)
            theBS[i].hasStarted = true;
        theMusic.Play();
    }

    // Update is called once per frame
   /* void Update()
    {
        if(!startPlaying)
        if (Input.anyKeyDown)
        {
            startPlaying = true;
            for(int i=0;i< theBS.Length;i++)
            theBS[i].hasStarted = true;
            theMusic.Play();
        }
    }*/
}
