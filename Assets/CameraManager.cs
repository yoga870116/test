﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    Transform player;
    public float cameraXPosMin, cameraXPosMax;
    public Vector2 offset;
    public float smoothSpeed;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FixedUpdate()
    {
        Vector3 newPos = new Vector3(player.position.x + offset.x, player.position.y + offset.y, -1); //Local vector get player position

        transform.position = Vector3.Lerp(transform.position, newPos, smoothSpeed * Time.deltaTime); //Set camera position smooth

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, cameraXPosMin, cameraXPosMax), transform.position.y, transform.position.z); //make clamp
    }
}
